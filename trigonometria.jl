#MAC0110 - MiniEP7
#Lucas Medeiros Cornetta - 6800685

# A função fact calcula o fatorial de n,
# e foi chamada nas três funções trigonométricas
function fact(n)
	if n == 0
		return 1
	elseif n == 1
		return 1
	else
		return n*fact(n-1)
	end
end

# A função bernoulli calcula o termo Bn,
# e foi chamada na função tangente
function bernoulli(n)
        if n == 1
                return 1/6
        else
                n *= 2
                A = Vector{Rational{BigInt}}(undef, n + 1)
                for m = 0 : n
                        A[m + 1] = 1 // (m + 1)
                        for j = m : -1 : 1
                                A[j] = j * (A[j] - A[j + 1])
                        end
                end
                return abs(A[1])
        end
end

function sin(x)::BigFloat
        prec = 1e-4
        i = 0
        sin::BigFloat = 0
        while i < 10
                termo::BigFloat = (-1)^i*x^(2*i+1)/fact(2*i+1)
                sin += termo
                if abs(termo) <= prec
                        return sin
                else
                        i += 1
                end
        end
        return sin
end

function cos(x)::BigFloat
        prec = 1e-4
        i = 0
        cos::BigFloat = 0
        while i < 10
                termo::BigFloat = (-1)^i*x^(2*i)/fact(2*i)
                cos += termo
                if abs(termo) <= prec
                        return cos
                else
                        i += 1
                end
        end
        return cos
end

function tan(x)::BigFloat
        prec = 1e-4
        i = 1
        tan::BigFloat = 0
        while i < 10
                termo::BigFloat = 2^(2*i)*(2^(2*i)-1)*bernoulli(i)*x^(2*i-1)/fact(2*i)
                tan += termo
                if abs(termo) <= prec
                        return tan
                else
                        i += 1
                end
        end
        return tan
end


function check_sin(value,x)
        prec = 1e-4
        if abs(Base.sin(x)-value) <= prec
                return true
        else
                return false
        end
end

function check_cos(value,x)
        prec = 1e-4
        if abs(Base.cos(x)-value) <= prec
                return true
        else
                return false
        end
end

function check_tan(value,x)
        prec = 1e-4
        if abs(Base.tan(x)-value) <= prec
                return true
        else
                return false
        end
end

function taylor_sin(x)::BigFloat
        prec = 1e-4
        i = 0
        sin::BigFloat = 0
        while i < 10
                termo::BigFloat = (-1)^i*x^(2*i+1)/fact(2*i+1)
                sin += termo
                if abs(termo) <= prec
                        return sin
                else
                        i += 1
                end
        end
        return sin
end

function taylor_cos(x)::BigFloat
        prec = 1e-4
        i = 0
        cos::BigFloat = 0
        while i < 10
                termo::BigFloat = (-1)^i*x^(2*i)/fact(2*i)
                cos += termo
                if abs(termo) <= prec
                        return cos
                else
                        i += 1
                end
        end
        return cos
end

function taylor_tan(x)::BigFloat
        prec = 1e-4
        i = 1
        tan::BigFloat = 0
        while i < 10
                termo::BigFloat = 2^(2*i)*(2^(2*i)-1)*bernoulli(i)*x^(2*i-1)/fact(2*i)
                tan += termo
                if abs(termo) <= prec
                        return tan
                else
                        i += 1
                end
        end
        return tan
end

using Test
function test()
	@test isapprox(taylor_sin(0),0) atol = 1e-4
	@test isapprox(taylor_sin(pi/2),1) atol = 1e-4
	@test isapprox(taylor_sin(pi/6),0.5) atol = 1e-4
	@test isapprox(taylor_cos(0),1) atol = 1e-4
	@test isapprox(taylor_cos(pi/2),0) atol = 1e-4
	@test isapprox(taylor_cos(pi/3),0.5) atol = 1e-4
	@test isapprox(taylor_tan(0),0) atol = 1e-4
	@test isapprox(taylor_tan(pi/6),sqrt(3)/3) atol = 1e-4
	println("Fim dos testes das funções da parte 1")
	@test check_sin(0.5, pi / 6)
	@test check_cos(0.5, pi / 3)
	@test check_tan(0, pi)
	@test !check_sin(42, pi)
	@test !check_cos(42, pi)
	@test !check_tan(42, pi / 4)
	println("Fim dos testes das funções da parte 2")
end

